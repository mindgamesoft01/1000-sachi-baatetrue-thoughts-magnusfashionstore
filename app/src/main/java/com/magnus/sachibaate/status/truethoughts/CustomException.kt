package com.magnus.sachibaate.status.truethoughts

class CustomException(message: String) : Exception(message)
